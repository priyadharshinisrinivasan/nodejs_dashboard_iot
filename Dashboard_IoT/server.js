const express = require('express');
const http = require('http');
const WebSocket = require('ws');
const moment = require('moment');
const path = require('path');
const iotHubClient = require('./IoThub/iot-hub');
process.env['Azure.IoT.IoTHub.ConnectionString'] = "HostName=IoTSpherehub1.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=DQjiX2XmHUHARgNmQWhNWZb9JXH6navKQeGRJmeCexw=";
//process.env['Azure.IoT.IoTHub.ConnectionString'] = "HostName=IoTSphereHub.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=C2jJZQaLAwIdUmX4C32VUQPXNXyts99ch4c70TfIBKk=";
//process.env['Azure.IoT.IoTHub.ConnectionString'] = "HostName=iotsphereclone.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=LOznMoqurcsSMk0fGKVAdtMfWQGXmkafjdV4I/ta0ow=";
process.env['Azure.IoT.IoTHub.ConsumerGroup'] = "iotdashboard";
const app = express();




app.use('/zone1', express.static(path.join(__dirname + '/zone1')));
app.use('/zone2', express.static(path.join(__dirname + '/zone2')));
app.use('/zone3', express.static(path.join(__dirname + '/zone3')));
app.use('/zone4', express.static(path.join(__dirname + '/zone4')));

const server = http.createServer(app);
const wss = new WebSocket.Server({ server });

// Broadcast to all.
wss.broadcast = function broadcast(data) {
    wss.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            try {
                console.log('sending data ' + data);
                client.send(data);
            } catch (e) {
                console.error(e);
            }
        }
    });
};



var iotHubReader = new iotHubClient(process.env['Azure.IoT.IoTHub.ConnectionString'], process.env['Azure.IoT.IoTHub.ConsumerGroup']);
console.log(process.env['Azure.IoT.IoTHub.ConnectionString']);
iotHubReader.startReadMessage(function (obj, date) {
    try {
        console.log(date);
        date = date || Date.now()
        wss.broadcast(JSON.stringify(Object.assign(obj, { time: moment.utc(date).format('hh:mm:ss') })));
    } catch (err) {
        console.log(obj);
        console.error(err);
    }
});

var port = normalizePort(process.env.PORT || '8080');
server.listen(port, function listening() {
    console.log('Listening on %d', server.address().port);
});

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}
